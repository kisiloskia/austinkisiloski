﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace BouncingBall
{
    public class Ball : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Vector2 position;
        private Vector2 speed;
        private Vector2 stage;
        private SoundEffect sound;
        private SoundEffect applause;

        public Vector2 Speed { get => speed; set => speed = value; }

        public Ball(Game game, 
            SpriteBatch spriteBatch, 
            Texture2D tex, 
            Vector2 position,
            Vector2 speed,
            Vector2 stage, 
            SoundEffect clickSound,
            SoundEffect applause
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position = position;
            this.speed = speed;
            this.stage = stage;
            this.sound = clickSound;
            this.applause = applause;
            
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex, position, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            position += speed;
            //Top Wall
            if(position.Y <= 0)
            {
                speed.Y = -speed.Y;
                sound.Play();
                
            }
            //Right Wall
            if(position.X > stage.X - tex.Width)
            {
                speed.X = -speed.X;
                sound.Play();
            }
            //Left Wall
            if(position.X < 0)
            {
                speed.X = -speed.X;
                sound.Play();
            }
            //Bottom Wall
            if(position.Y > stage.Y)
            {
                //speed.Y = -speed.Y;
                applause.Play();
                this.Enabled = false;
            }
            base.Update(gameTime);
            
        }
        public Rectangle getBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, tex.Width, tex.Height);
        }
    }
}
