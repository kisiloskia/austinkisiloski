﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

using System;

namespace BouncingBall
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Ball ball;
        private Bat bat;
        private CollisionDetction cm;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth += graphics.PreferredBackBufferWidth;
            graphics.PreferredBackBufferHeight += graphics.PreferredBackBufferHeight;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
           
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            Texture2D ballTex = this.Content.Load<Texture2D>("Images/Ball");
            Vector2 stage = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            Vector2 ballInitPos = new Vector2(stage.X / 2 - ballTex.Width/2, stage.Y / 2 - ballTex.Height/2);
            Vector2 ballSpeed = new Vector2(5, -5);

            SoundEffect clickSound = this.Content.Load<SoundEffect>("Music/click");
            SoundEffect applause = this.Content.Load<SoundEffect>("Music/applause1");
            ball = new Ball(this, spriteBatch, ballTex, ballInitPos, ballSpeed, stage, clickSound, applause);
            this.Components.Add(ball);

            Texture2D batTex = this.Content.Load<Texture2D>("Images/Bat");
            Vector2 batInitPos = new Vector2(stage.X / 2 - ballTex.Width / 2, stage.Y - 100);
            Vector2 batSpeed = new Vector2(6, 0);
            bat = new Bat(this, spriteBatch, batTex, batInitPos, batSpeed, stage);
            this.Components.Add(bat);

            cm = new CollisionDetction(this, ball, bat);
            this.Components.Add(cm);

            //background music
            Song song = this.Content.Load<Song>("Music/chimes");
            MediaPlayer.IsRepeating = true;
            //MediaPlayer.Play(song);
            //Random r = new Random();

            //for (int i = 0; i < 10; i++)
            //{
            //    ball = new Ball(this, spriteBatch, ballTex, new Vector2(r.Next(0, (int)stage.Y)), ballSpeed, stage);
            //    this.Components.Add(ball);
            //}
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
