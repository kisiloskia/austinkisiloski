﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace BouncingBall
{
    public class CollisionDetction : GameComponent
    {
        private Ball ball;
        private Bat bat;
        public CollisionDetction(Game game, Ball ball, Bat bat) : base(game)
        {
            this.ball = ball;
            this.bat = bat;
        }

        public override void Update(GameTime gameTime)
        {
            Rectangle ballRect = ball.getBound();
            Rectangle batRect = bat.getBound();
            float X = ball.Speed.X;
            float Y = ball.Speed.Y;

            if(ballRect.Intersects(batRect))
            {
              
                ball.Speed = new Vector2(ball.Speed.X,-ball.Speed.Y);
                
            }
            
            base.Update(gameTime);
        }
    }
}
